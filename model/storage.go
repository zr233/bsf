package model

type Storage interface {
	Open() (err error)
	Close() (err error)
	GetConfigPtr() interface{}
}
type StorageCollection interface {
	GetHbase() Storage
	GetRedis() Storage
	GetSqlCluster() Storage
	GetZookeeper() Storage
	GetKafka() Storage
}
