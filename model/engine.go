package model

import "github.com/sirupsen/logrus"

type Engine interface {
	Logger() logrus.FieldLogger
}
