package model

import "github.com/ZR233/gconfig"

type Config struct {
	Hbase      *gconfig.Hbase
	Zookeeper  *gconfig.Zookeeper
	Redis      *gconfig.Redis
	SqlCluster *gconfig.SqlCluster
	KafkaAddrs []string
}
