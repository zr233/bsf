module gitee.com/zr233/bsf

go 1.14

require (
	github.com/ZR233/gconfig v1.1.5
	github.com/ZR233/goutils v1.4.12
	github.com/gin-gonic/gin v1.6.3
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	gorm.io/driver/postgres v1.0.6
	gorm.io/gorm v1.20.9
	gorm.io/plugin/dbresolver v1.0.1
)
