package test

import (
	bbb "gitee.com/zr233/bsf"
	"testing"
)

type TestCon struct {
	*bbb.ControllerBase
}

func (t *TestCon) HttpGetHello() (f bbb.ControllerFunc) {
	return func() (err error) {
		println("seccess")
		return nil
	}
}

func Test_test(t *testing.T) {
	c := &TestCon{
		&bbb.ControllerBase{},
	}

	e := bbb.NewDefaultEngine()
	e.AutoRegisterController("/", c)
}
