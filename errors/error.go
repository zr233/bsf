package errors

import "errors"

type ErrorCode int

const ErrorCode_Unknown ErrorCode = 9999999999
const (
	CodeParam ErrorCode = 60000
)

//StandardError 标准错误，包含错误码和错误信息
type StandardError struct {
	Code ErrorCode
	File string
	error
}

func NewParamErr(err error) error {
	return NewFromError(err, CodeParam)
}

func FromError(err error) (stdErr *StandardError) {
	stdErr = NewFromError(err, ErrorCode_Unknown)
	return
}

func NewFromError(err error, code ErrorCode) (stdErr *StandardError) {
	if err != nil {
		ok := false
		errTemp := err
		for {
			if stdErr, ok = errTemp.(*StandardError); ok {
				break
			}

			errTemp = errors.Unwrap(errTemp)
			if errTemp == nil {
				break
			}
		}
		if stdErr == nil {
			stdErr = &StandardError{
				Code:  code,
				File:  "",
				error: err,
			}
		}
		return
	} else {
		return nil
	}
}
