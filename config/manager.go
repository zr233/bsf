package config

import (
	"fmt"
	"gitee.com/zr233/bsf/model"
	"github.com/ZR233/gconfig"
	"github.com/ZR233/gconfig/consul"
)

type Manager struct {
	*gconfig.Config
	DB     gconfig.DB
	config model.Config
	engine model.Engine
}

func NewManager(engine model.Engine) (m *Manager, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("config center init fail: \n%w", err)
		}
	}()

	db, err := consul.NewDBConsul(nil)
	if err != nil {
		return
	}
	c := gconfig.NewConfig(db)
	m = &Manager{DB: db, engine: engine, Config: c}
	return
}
